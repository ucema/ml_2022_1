print("Hola")

# Se define la variable edad con el valor de retorno de input()
edad = input("Ingrese su edad:")
print(edad)

# Colecciones: list [], tuple (), dict {}, set

n = ['a', 'b', 'c']
print(n)
print(n[0]) # Muestra el primer elemento

n.append('d')
print(n)

t = ('a', 'b', 'c', )
print(t)

print(type(t))

t1 = ('a', )
print(type(t1))

# key: value
d = {1: 'uno', 2: 'dos', 3: 'tres'}
print(d[2])

d2 = {'uno': 'one', 'dos': 'two', 'tres': 'three'}
print(d2['dos'])

print('------------------------')
# tienen un iterador
for x in n:
    print(x)

print('------------------------')
for x in d2:
    print(x, d2[x])

# Set: conjunto (No tiene orden, No permite elementos repetidos)
s = set([1,2,3,4,5,2,5,2,7,4])
print(s)
print(d2)

s1 = {1,2,3,4,4,2}
print(s1)

print('------------------------')
print(set(d2.keys()))
