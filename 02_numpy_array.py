# Vectores: ndarray
import numpy as np

vector_a = np.array([1,2,3,4,505])
print(vector_a)

vector_b = np.array([1.0, 1.1, 1.2, 1.3, 1.4])
print(vector_b)

escalar = 2 * vector_b
print(escalar)

sumar_vectors = vector_a + vector_b
print(sumar_vectors)

mult = vector_a * vector_b
print(mult)

# Shape o forma de la matriz
print(vector_a.shape)       # (5,) un tupla con las dimensiones de la matriz

print('Dimension ', vector_a.ndim)
print('Cantidad de elementos: ', vector_a.size)

vector_c = np.arange(1, 26, 1)      # [1, 26)
print(vector_c)

vector_d = vector_c.reshape(5, 5)
print(vector_d)

print()
vector_e = np.arange(1, 9, 1)
print(vector_e)

vector_f = vector_e.reshape(2, 2, 2)
print(vector_f)

print()
vector_d[1][3] = 100
vector_d[4, 1] = 200
print(vector_d)

vector_d[2] = np.array([9,8,7,6,5])
print(vector_d)

# Task: Modificar la diagonal principal con 0
