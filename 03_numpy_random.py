#
# 2022-03-23
#
import numpy as np

np.random.seed(0)

print(np.random.rand()) # Entre 0 y 1

# Un numero real entre -5 y 10      10 - -5 = 15
numero_a = np.random.rand() * 15 + -5 
print(numero_a)

vector_c = np.arange(1, 21, 1)      # [1, 26)
print(vector_c)

np.random.shuffle(vector_c)
print(vector_c)

numero_b = np.random.randint(100, 120)
print(numero_b)
