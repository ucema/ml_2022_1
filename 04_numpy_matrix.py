#
# 2022-03-23
#
import numpy as np

# lista_a = []
# for n in range(0, 16):
#     lista_a.append(n)

# print(lista_a)

lista_a = [ n for n in range(16) ]
print(lista_a)


lista_b = [ np.random.randint(0, 10) for _ in range(16) ]
print(lista_b)

matriz_a = np.array(lista_b).reshape(4, 4)
print(matriz_a)

# matriz_a = np.array(lista_b).reshape(2, 2, 4)
# print(matriz_a)

lista_c = [ np.random.randint(0, 10) for _ in range(16) ]
matriz_c = np.array(lista_c).reshape(4, 4)
print(matriz_c)

print()
print(matriz_a + matriz_c)

print()
print(matriz_a * matriz_c)

print()
# Task: Investigar producto vectorial

matriz_0 = np.zeros((4, 4))
print(matriz_0)

print()
x_distantes = np.linspace(100, 500, 40)
print(x_distantes)

print()
print(matriz_a)
print(matriz_a.transpose())

print()
print(np.linalg.det(matriz_a))

print()
suma_filas = matriz_a.sum(axis=0)
print(suma_filas)
