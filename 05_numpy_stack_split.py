#
# 2022-04-06
#
import numpy as np

a = np.round(np.random.random((2, 3)) * 20).astype("int32")
b = np.round(np.random.random((2, 4)) * 20).astype("int32")
c = np.round(np.random.random((2, 5)) * 20).astype("int32")

print(a, "\n\n", b, "\n\n", c)

# Juntar matrices una al lado de la otra
h = np.hstack((a, b, c))

print("\n\n", h)

print("\n", "*" * 40)

a = np.round(np.random.random((3, 3)) * 20).astype("int32")
b = np.round(np.random.random((5, 3)) * 20).astype("int32")
c = np.round(np.random.random((2, 3)) * 20).astype("int32")

print(a, "\n\n", b, "\n\n", c)

v = np.vstack((a, b, c))
print("\n\n", v)

print("\n", "*" * 40)

s = np.hsplit(h, 3) # Prestar atencion que la cantidad de grupos debe ser divisor de la cantidad de columnas de h
for m in s:
    print(m)
    print()

print("\n", "*" * 40)

s = np.vsplit(v, 2)
for m in s:
    print(m)
    print()
