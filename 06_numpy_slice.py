#
# 2022-04-06
#
import numpy as np

# a = [10,20,30,40,50,60,70,80]
# a
# [10, 20, 30, 40, 50, 60, 70, 80]
# a[1]
# 20  
# a[0]
# 10  
# len(a)
# 8   
# a[0:3]              
# [10, 20, 30]
# a[:3]
# [10, 20, 30]
# a[3:6]
# [40, 50, 60]
# a[::2]
# [10, 30, 50, 70]
# a[1::2]
# [20, 40, 60, 80]
# a[1:4:2]
# [20, 40]
# a[::-1]
# [80, 70, 60, 50, 40, 30, 20, 10]
# a[0::-1]
# [10]
# a[-1]
# 80
# a[-2]

a = np.round(np.random.random((10, 6)) * 20).astype("int32")
print(a)

print()
a.sort(axis=1)
print(a)

print()
print("*" * 50)
print()

elemento = a[2, 4]
print(elemento)

fila = a[2]
print(fila)

#    [Filas, Columnas]
x = a[0:4,1:3]
print(x)

print()
print("*" * 50)
print()

r = a[a % 3 == 0]
print(r)


print()
print("*" * 50)
print()

b = np.round(np.random.random((3, 2)) * 20).astype("int32")
print(b)
print()

b = b.ravel()
print(b)
print()

print()
print("*" * 50)
print()

c = np.round(np.random.random((5, 4)) * 20 - 10).astype("int32")
print(c)
print()
print(np.sin(c))
print()
print(np.abs(c))
