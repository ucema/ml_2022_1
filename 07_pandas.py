#
# 2022-04-06
#
# Dataframe: """Una tabla de excel"""
# Series: una fila de datos, como un vector. Tiene un indice.

import pandas as pd

data = {
    "Pais": ["Argentina", "Brasil", "Chile", "Peru", "Uruguay"],
    "Capital": ["CABA", "Brasilia", "Santiago", "Lima", "Montevideo"],
    "Poblacion": [46, 110, 20, 62, 20]
}

paises = pd.DataFrame(data)
paises.index = ["AR", "BR", "CH", "PE", "UR"]
print(paises)

print()
a = paises.iloc[1]
print(a)
print()
print(a["Pais"])
print(a.index)
print()
print(paises.index)
print()
b = paises.loc["AR"]
print(b)

print()
print('Desde CSV:')
paises_csv = pd.read_csv("paises.csv")
print(paises_csv)

print()
print(paises_csv.describe())
