#
# 2022-04-13
#
import pandas as pd

paises_csv = pd.read_csv("paises.csv")
print(paises_csv)

pob = paises_csv[['Pais', 'Poblacion']]     # Los nombres son case-sensitive
print(pob)

print()
print('Los 3 primeros elementos:')
print(pob.head(3))

print()
print('Los 3 ultimos elementos:')
print(pob.tail(3))


print()

sales = pd.read_csv('vgsales.csv')
print(sales.head())
print()
print(sales.describe())
print()

# Obtengo los Index de las filas con Year en null
rows_with_nulls = sales['Year'].isnull()
print(rows_with_nulls.count())

print()
# Obtengo el dataframe a partir de los indices de filas en null
df_with_nulls = sales[rows_with_nulls]
print(df_with_nulls)

print()
sales_without_nulls = sales.dropna()
print(sales_without_nulls)

print()
sales_filled = sales.fillna(2000)
print(sales_filled.describe())

print()
# p = sales_without_nulls.pivot(index='Genre', columns='Year', values='Global_Sales')
# print(p)

print()
df = pd.DataFrame({
        'foo': ['one', 'one', 'one', 'two', 'two', 'two'],
        'bar': ['A', 'B', 'C', 'A', 'B', 'C'],
        'baz': [1, 2, 3, 4, 5, 6],
        'zoo': ['x', 'y', 'z', 'q', 'w', 't']
    })
print(df.pivot(index='foo', columns='bar', values='baz'))