import matplotlib.pyplot as plt
import numpy as np

# Ejemplo 1:
# x = [1, 2, 3, 4, 5]
# y = [1, 4, 8, 16, 32]
# y2 = [1, 10, 20, 30, 40]

# plt.plot(x, y, "bo")  # 2^x
# plt.plot(x, y2, "rx")  # 2^x
# plt.ylabel("eje y")
# plt.show()


# Ejemplo 2
# t = np.arange(0, 5, 0.2)    # 0-0.2-0.4-0.6-....-4.8
# y1 = t**2
# y2 = t**3

# plt.plot(t, y1, "r--")
# plt.plot(t, y2, "g^")

# plt.show()


# Ejemplo 3
q = 50
data = {
    "a": np.arange(q),
    "c": np.random.randint(0, q, q),
    "d": np.random.randn(q)
}
data["b"] = data["a"] + 10 * np.random.randn(q)
data["d"] = np.abs(data["d"]) * 100

print(data)

plt.scatter("a", "b", c="c", s="d", data=data)
# Parametro 1: x
# Parametro 2: y
# Parametro c: Colores (numeros enteros o letras de colores)
# Parametro s: Tamaño (size)
plt.show()
