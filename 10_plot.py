import matplotlib.pyplot as plt
import numpy as np

categories = ["Votos A", "Votos B", "Votos C"]
quantity = [48, 17, 76]

studies_univ = [42, 3, 96]


plt.figure(figsize=(6, 6))

plt.subplot(2, 2, 1)
plt.bar(categories, studies_univ)

plt.subplot(2, 2, 2)
plt.bar(categories, quantity)

plt.subplot(2, 2, 3)
plt.plot([1, 2, 3, 4, 5], [1, 4, 8, 16, 32])

plt.suptitle("Votos")
plt.show()