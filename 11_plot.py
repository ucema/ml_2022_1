import matplotlib.pyplot as plt
import numpy as np

def compute(t):
    return np.exp(-t) * np.cos(2*np.pi*t)


t1 = np.arange(0, 5, 0.1)
t2 = np.arange(0, 5, 0.02)

plt.figure()
plt.subplot(2, 1, 1)
plt.plot(t1, compute(t1), "bo", t2, compute(t2), "k")

plt.subplot(2, 1, 2)
plt.plot(t2, np.cos(2*np.pi*t2), "g--")

plt.show()
