import matplotlib.pyplot as plt
import numpy as np


mu, sigma = 100, 15
x = mu + sigma * np.random.randn(10000)

n, bins, p = plt.hist(x, 50, density=True, facecolor="c")

# print(p[20].__dict__)

x1 = bins[10]
y1 = n[10]
print(x1, y1)
plt.annotate(
    "Cuidado",
    xy=(x1, y1),
    xytext=(50, 0.015),
    arrowprops=dict(facecolor="black", shrink=0.05)
)

plt.title("Histograma")
plt.text(120, 0.025, r"$\mu=100, \sigma=15$")
plt.grid(True)

plt.show()
